#!/usr/bin/env lua
if tonumber((string.gsub(_VERSION, "Lua ", ""))) < 5.3 then error("Require Lua 5.3"); end
--[[
https://gitlab.com/mdkmde/test_lua/ - matthias (at) koerpermagie.de

Copyright (c) MIT-License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local error, load, pcall, type = error, load, pcall, type
local io = {open = io.open, write = io.write}
local string = {find = string.find, format = string.format, gmatch = string.gmatch, gsub = string.gsub}
local table = {concat = table.concat, unpack = table.unpack}
local package_path = string.gsub(string.format("%s;", package.path), ";;+", ";")
_ENV = nil

local t_checks = {t_done = {}, t_fail = {}, t_warn = {}}
local err_write = function(is_name, is_object, is_level, is_error_level, ...)
  if not(t_checks.t_fail[is_name]) and not(t_checks.t_warn[is_name]) then
    io.write(string.format("{ Issue in test case: \"%s\":\n", is_name))
  end
  io.write(string.format(" |%s%s -> %s[%s] - %s", is_level, is_error_level, is_name, string.gsub(is_object, "%.$", ""), string.format(...)))
end

local checkResult, checkResultDetail
checkResultDetail = function(is_name, it_expect, it_got, is_level, is_object, k)
  if k > #it_expect then
    return
  end
  local v = it_expect[k]
  local s_object = string.format("%s%s.", is_object, k)
  t_checks.t_done[is_name] = (t_checks.t_done[is_name] or 0) + 1
  if (type(v) == "string") and (v == "nil") and (not(type(it_got[k]) == "string") or not(it_got[k] == "nil")) then
    v = nil
  end
  if not(type(v) == type(it_got[k])) then
    err_write(is_name, s_object, is_level, "FAIL", "result is not the same type, expected: \"%s\" got: \"%s\"\n", type(v), type(it_got[k]))
    t_checks.t_fail[is_name] = (t_checks.t_fail[is_name] or 0) + 1
  elseif (type(v) == "boolean") or (type(v) == "number") or (type(v) == "string") or (type(v) == "nil") then
    if not(v == it_got[k]) then
      err_write(is_name, s_object, is_level, "FAIL", "result is not equal, expected: \"%s\" got: \"%s\"\n", v, it_got[k])
      t_checks.t_fail[is_name] = (t_checks.t_fail[is_name] or 0) + 1
    end
  elseif (type(v) == "table") then
    t_checks.t_done[is_name] = t_checks.t_done[is_name] - 1
    checkResult(is_name, v, it_got[k], string.format("%s-", is_level), s_object)
  else -- (type(v) => function, userdata, thread
    err_write(is_name, s_object, is_level, "WARN", "result is type: \"%s\", cannot compare\n", type(v))
    t_checks.t_warn[is_name] = (t_checks.t_warn[is_name] or 0) + 1
  end
  checkResultDetail(is_name, it_expect, it_got, is_level, is_object, k + 1)
end

checkResult = function(is_name, it_expect, it_got, is_level, is_object)
  is_object = is_object or ""
  it_expect = it_expect or {}
  if not(#it_expect == #it_got) then
    err_write(is_name, is_object, is_level, "FAIL", "result count is not equal, expected: \"%d\" got: \"%d\"\n", #it_expect, #it_got)
    t_checks.t_fail[is_name] = (t_checks.t_fail[is_name] or 0) + 1
    t_checks.t_done[is_name] = (t_checks.t_done[is_name] or 0) + 1
  elseif #it_expect == 0 then
    t_checks.t_done[is_name] = (t_checks.t_done[is_name] or 0) + 1
  else
    checkResultDetail(is_name, it_expect, it_got, is_level, is_object, 1)
  end
end

local checkLocalFunction
checkLocalFunction = function(is_module, iot_local_function, i_iter)
  local s_local_function_name = i_iter()
  if s_local_function_name then
    if string.find(is_module, string.format("[\r\n]local[%%w_, ]*[, ]%s[^%%w_]", s_local_function_name)) then
      iot_local_function[#iot_local_function + 1] = string.format("%s=%s,", s_local_function_name, s_local_function_name)
    end
    checkLocalFunction(is_module, iot_local_function, i_iter)
  end
  return iot_local_function
end

local getFunctionNames
getFunctionNames = function(is_module, iot_local_function, it_function_pattern, in_pos)
  local s_pattern = it_function_pattern[in_pos]
  if s_pattern then
    iot_local_function = checkLocalFunction(is_module, iot_local_function, string.gmatch(is_module, s_pattern))
    return getFunctionNames(is_module, iot_local_function, it_function_pattern, in_pos + 1)
  end
  return iot_local_function
end

local t_function_pattern = {
  "[\r\n]function *([^\r\n%( ]+) *%(",
  "[\r\n]local +function *([^\r\n%( ]+) *%(",
  "[\r\n]([^\r\n%( =]+) * = *function *%(",
  "[\r\n]local +([^\r\n%( =]+) * = *function *%(",
}
local require = function(is_module_name)
  local fr_module
  string.gsub(package_path, "([^;]+);", function(is_path)
    if not(fr_module) then
      local s_module_path_name = string.gsub(is_path, "%?", is_module_name)
      local f = io.open(s_module_path_name)
      if f then
        fr_module = f
      end
    end
  end)
  if not(fr_module) then
    error(string.format("Cannot find module: %s", is_module_name))
  end
  local s_module = string.gsub(fr_module:read("a"), "^#[^\r\n]*[\r\n]*", "")
  fr_module:close()
  local t_local_function = getFunctionNames(s_module, {}, t_function_pattern, 1)
  local s_module_internal, n_found = string.gsub(s_module, "([\r\n])return ", string.format("%%1return {%s},", table.concat(t_local_function)))
  if n_found == 0 then
    s_module_internal = string.gsub(s_module, "([\r\n])([^\r\n]*)$", string.format("%%1%%2%%1return {%s}", table.concat(t_local_function)))
  end
  local initModule, s_err = load(s_module_internal, is_module_name)
  if s_err then
    error(string.format("Cannot load module %s: %s", is_module_name, s_err))
  end
  local t_module_internal, t_module = initModule()
  return t_module, t_module_internal
end

local run = function(is_name, i_function, it_arg, it_result)
  local t_result = {pcall(i_function, table.unpack(it_arg))}
  local nextFreeName = function() end
  nextFreeName = function(is_name, in_name)
    if not(t_checks.t_done[is_name]) then
      return is_name
    end
    return nextFreeName(string.format("%s (%d)", string.gsub(is_name, " %([0-9][0-9]*%)$", ""), in_name), in_name + 1)
  end
  local s_name = nextFreeName(is_name, 1)
  if not(t_result[1]) then
    err_write(s_name, "", "-", "FAIL", "Cannot call \"%s\": %q\n", i_function, t_result[2])
    t_checks.t_fail[s_name] = (t_checks.t_fail[s_name] or 0) + 1
    t_checks.t_done[s_name] = (t_checks.t_done[s_name] or 0) + 1
  else
    checkResult(s_name, it_result, {table.unpack(t_result, 2)}, "-")
  end
  t_checks.n_done = (t_checks.n_done or 0) + (t_checks.t_done[s_name] or 0)
  t_checks.n_fail = (t_checks.n_fail or 0) + (t_checks.t_fail[s_name] or 0)
  t_checks.n_warn = (t_checks.n_warn or 0) + (t_checks.t_warn[s_name] or 0)
  if t_checks.t_fail[s_name] or t_checks.t_warn[s_name] then
    io.write(string.format("} Checks done: %d, fail: %d, warn: %d\n-\n", t_checks.t_done[s_name] or 0, t_checks.t_fail[s_name] or 0, t_checks.t_warn[s_name] or 0))
  end
end

local summary = function()
  io.write(string.format("--\nCheck summary - done: %d, fail: %d, warn: %d\n", t_checks.n_done or 0, t_checks.n_fail or 0, t_checks.n_warn or 0))
end

return {
  require = require, --[[ returns module, special module: loads local functions as global for better testing ]]
  run = run, --[[ test: name, function, arg_table, result_table ]]
  summary = summary, --[[ print some test summary stat: done, fail, warn ]]
}

