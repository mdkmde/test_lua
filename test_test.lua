local myFunc = function(i_a, i_b)
  if (type(i_a) == "string") and (type(i_b) == "string") then
    return string.format("%s%s", i_a, i_b)
  elseif (type(i_a) == "number") and (type(i_b) == "number") then
    return {i_a + i_b, i_a - i_b}
  elseif (type(i_a) == "boolean") and (type(i_b) == "boolean") then
    return i_a and i_b, i_a or i_b
  elseif (type(i_a) == "nil") and (type(i_b) == "nil") then
    return "nil"
  elseif not(type(i_a) == type(i_b)) then
    return nil, "Don't know"
  end
  return
end

local runTest = function(it_test)
  it_test.run("strings", myFunc, {"a", "b"}, {"ab"})
  it_test.run("numbers", myFunc, {2, 1}, {{3, 1}})
  it_test.run("boolean", myFunc, {true, false}, {false, true})
  it_test.run("nil", myFunc, {}, {"nil"})
  it_test.run("other", myFunc, {"a"}, {"nil", "Don't know"})
  it_test.run("other", myFunc, {"a", 1}, {nil, "Don't know"})
  it_test.run("tables", myFunc, {{}, {}}, {})
  it_test.run("tables", myFunc, {{}, {}})

  it_test.summary()
end

local test = require("test")
if not(_G.loaded_test_test) then
  _G.loaded_test_test = true
  local _, test_internal = test.require("test_test")
  test.run("test_internal", test_internal.runTest, {test}, {})
end

